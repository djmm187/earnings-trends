snippet = """
<tr>
<td>
<a href="http://www.nasdaq.com/earnings/report/agen" id="two_column_main_content_Repeaterunconfirmed_companyname_0">Agenus Inc. (AGEN) <br/><b>Market Cap: $521.37M</b></a>
</td>
<td>
                                                                    07/27/2017
                                                            </td>
<td>
                                                                    Jun 2017
                                                            </td>
<td>
                                                                    $-0.36
                                                            </td>
<td>
                                                                    2
                                                            </td>
<td style="">
                                                                    07/28/2016
                                                            </td>
<td>
                                                                    $-0.24
                                                            </td>
<td style="display:none">
<span style="color:green">Met</span>
</td>
</tr>
"""

import earnings_schedule
from bs4 import BeautifulSoup

soup = BeautifulSoup(snippet, "html.parser")
earnings_schedule.extract_meta(soup)